<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Relación muchos a muchos</title>
  </head>
  <body>
    <h1>Relación muchos a muchos</h1>

    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h3>El alumno: <span class="badge bg-secondary">{{ $alumno->nombre }}</span> cursa las materias:</h3>
                <table class="table table-striped table-hover">
                    <thead class="bg-primary text-white">
                        <th>MATERIAS</th>
                    </thead>
                    <tbody>
                        @foreach($alumno->materias as $registro)
                        <tr>
                            <td>{{ $registro->nombre }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-auto">
                <h3>La matria con sus alumnos: <span class="badge bg-secondary">{{ $materia->nombre }}</span></h3>
                <table class="table table-striped table-hover">
                    <thead class="bg-primary text-white">
                        <th>ALUMNOS</th>
                    </thead>
                    <tbody>
                        @foreach($materia->alumnos as $alumno)
                        <tr>
                            <td>{{ $alumno->nombre }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  </body>
</html>