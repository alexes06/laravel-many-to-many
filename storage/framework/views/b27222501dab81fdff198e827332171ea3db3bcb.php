<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Relación muchos a muchos</title>
  </head>
  <body>
    <h1>Relación muchos a muchos</h1>

    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h3>El alumno: <span class="badge bg-secondary"><?php echo e($alumno->nombre); ?></span> cursa las materias:</h3>
                <table class="table table-striped table-hover">
                    <thead class="bg-primary text-white">
                        <th>MATERIAS</th>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $alumno->materias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $registro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($registro->nombre); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-auto">
                <h3>La matria con sus alumnos: <span class="badge bg-secondary"><?php echo e($materia->nombre); ?></span></h3>
                <table class="table table-striped table-hover">
                    <thead class="bg-primary text-white">
                        <th>ALUMNOS</th>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $materia->alumnos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $alumno): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($alumno->nombre); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  </body>
</html><?php /**PATH /mnt/bd148f3d-a4ba-415f-b04c-d8a0b94677f5/alejandro_ubuntu_2020/desarrollo/laravel/sep/30/manytomany/resources/views/welcome.blade.php ENDPATH**/ ?>